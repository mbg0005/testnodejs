import { PrismaClient } from '@prisma/client';
const prisma = new PrismaClient();
// const mysql = require('mysql');

// const connection = mysql.createConnection({
//   host: 'mysql',
//   user: 'root',
//   password: 'mbg0001',
//   database: 'testnodejs'
// });

// const Users : any = getUsersPost();

const resolvers = {
  users: async (_: any) => {
    //return Users;
    return prisma.user.findMany({include:{posts:true}});
  },
  user: async ({ id }: any) => {
    return prisma.user.findUnique({where:{id:id},include:{posts:true}});
  },
  updateEmail: async ({ id, email }: any) => {
    return prisma.user.update({where:{id:id},data:{email:email}});
  },
  createUser: async ({ name, email}: any) => {
    return prisma.user.create({data:{name:name,email:email}});
  },
  createPost: async ({ title, published, link, authorId}: any) => {
    return prisma.post.create({data:{title:title,published:published,link:link,authorId:authorId}, include:{author:true}});
  },
  deletePost: async ({id}: any) => {
    return prisma.post.delete({where:{id:id}});
  },
  createUserPost: async ({ name, email, title, published, link}: any) => {
    return prisma.user.create({data:{name:name,email:email,posts:{create:{title:title,published:published,link:link}}}, include:{posts:true}});
  }
};

// prisma使用なし
// async function getUsersPost() {
//   const results : any = await getUsers();

//   for (let i=0;i <results.length;i++){
//       var id = results[i]['id'];
//       const result = await getPosts(id);
//       results[i]['posts'] = result;
//     };

//   return results;
// };

// function getUsers() {
//   return new Promise(function(resolve) {
//     connection.query('select * from User', function (error: any, results: unknown, fields: any) {
//       if (error) throw error;
//       resolve(results);
//     });
//   });
// };

// function getPosts(id: any) {
//   return new Promise(function(resolve){
//     connection.query('select * from Post Where author=?',[id], function (error: any, results: unknown, fields: any) {
//       if (error) throw error;
//         resolve(results);
//     });
//   });
// };

module.exports = resolvers;