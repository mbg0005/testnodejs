import { buildSchema } from "graphql";

const schema = buildSchema(`
    type Query {
        users: [User!]!,
        user(id: Int!): User!
    }

    type Mutation {
        updateEmail(id: Int!, email: String!): User!,
        createUser(name: String!, email: String): User!,
        createPost(title: String!, published: Boolean!, link: String, authorId: Int!): Post!,
        deletePost(id: Int!): Post,
        createUserPost(name: String!, email: String, title: String!, published: Boolean!, link: String): User!
    }

    type User {
        id: ID!
        name: String!
        email: String
        posts: [Post!]
    }

    type Post {
        id: ID!
        title: String!
        published: Boolean!
        link: String
        author: User!
        authorId: Int!
    }
`);

module.exports = schema;