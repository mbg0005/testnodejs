"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var graphql_1 = require("graphql");
var schema = graphql_1.buildSchema("\n    type Query {\n        users: [User!]!,\n        user(id: Int!): User!\n    }\n\n    type Mutation {\n        updateEmail(id: Int!, email: String!): User!,\n        createUser(name: String!, email: String): User!,\n        createPost(title: String!, published: Boolean!, link: String, authorId: Int!): Post!,\n        deletePost(id: Int!): Post,\n        createUserPost(name: String!, email: String, title: String!, published: Boolean!, link: String): User!\n    }\n\n    type User {\n        id: ID!\n        name: String!\n        email: String\n        posts: [Post!]\n    }\n\n    type Post {\n        id: ID!\n        title: String!\n        published: Boolean!\n        link: String\n        author: User!\n        authorId: Int!\n    }\n");
module.exports = schema;
