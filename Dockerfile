FROM node:lts

WORKDIR /src

RUN npm install -D graphql express express-graphql
RUN npm install -D prisma @prisma/client
RUN npm install -D ts-node typescript @types/node
RUN npm install -D graphql-playground-middleware-express

EXPOSE 3000